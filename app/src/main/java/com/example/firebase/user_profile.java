package com.example.firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;

public class user_profile extends AppCompatActivity {
DatabaseReference mdatabase;
DatabaseReference friendrequest;
FirebaseUser currentuser;
ImageView image;
TextView name;
TextView status;
String Dname;
String Dstatus;
String Dimage;
ProgressDialog diag;
String id;
String currentstate="not friends";
Button button;
Button button1;
DatabaseReference Mfriend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        image=findViewById(R.id.imageview19);
        name=findViewById(R.id.name19);
        status=findViewById(R.id.status19);
        id=getIntent().getStringExtra("user_id");
        mdatabase=FirebaseDatabase.getInstance().getReference().child("users").child(id);
        diag=new ProgressDialog(this);
        diag.setTitle("Profile");
        diag.setMessage("Loading your profile information");
        diag.setCanceledOnTouchOutside(false);
      //  diag.show();
      button=findViewById(R.id.send);
      button1=findViewById(R.id.decline);
      button1.setVisibility(View.GONE);

        mdatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             Dname=   dataSnapshot.child("Display").getValue().toString();
             Dstatus=dataSnapshot.child("Status").getValue().toString();
             Dimage=dataSnapshot.child("image").getValue().toString();
             if(!Dimage.equals("default")){

                 Picasso.get().load(Dimage).into(image);
             }
             else{
                 Picasso.get().load(R.drawable.images3).into(image);
             }

                name.setText(Dname);
                status.setText(Dstatus);
                currentuser = FirebaseAuth.getInstance().getCurrentUser();
                friendrequest=FirebaseDatabase.getInstance().getReference().child("Friendrequests");
                if(currentuser.getUid().equals(id)){
                    button1.setVisibility(View.GONE);
                    button.setVisibility(View.GONE);

                }
                else {
                    friendrequest.child(currentuser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.hasChild(id)) {
                                String request_type = dataSnapshot.child(id).child("request_type").getValue().toString();
                                if (request_type.equals("recieved")) {
                                    button1.setVisibility(View.VISIBLE);
                                    button.setText("accept friend request");
                                    button1.setText("decline friend request");
                                    currentstate = "req_recieved";
                                } else if (request_type.equals("sent")) {
                                    currentstate = "req_sent";
                                    button.setText("cancel friend request");

                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                 if(currentstate.equals("not friends")){
                     Mfriend=FirebaseDatabase.getInstance().getReference().child("friends");

                     Mfriend.child(currentuser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                         @Override
                         public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                             if(dataSnapshot.hasChild(id)){
                                 currentstate="friends";
                                 button.setText("unfriend this person");
                                 button1.setVisibility(View.GONE);
                               //  Toast.makeText(user_profile.this, currentstate, Toast.LENGTH_SHORT).show();
                             }
                         }

                         @Override
                         public void onCancelled(@NonNull DatabaseError databaseError) {

                         }
                     });

                 }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void send(View view) {
        currentuser = FirebaseAuth.getInstance().getCurrentUser();
        friendrequest = FirebaseDatabase.getInstance().getReference().child("Friendrequests");
        if (currentstate.equals("not friends")) {
            friendrequest.child(currentuser.getUid()).child(id).child("request_type").setValue("sent").addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        friendrequest.child(id).child(currentuser.getUid()).child("request_type").setValue("recieved").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                //    Toast.makeText(user_profile.this, "works well", Toast.LENGTH_SHORT).show();

                                currentstate="req_sent";

                                   button.setText("cancel friend request");
                                }
                                else{
                                    Toast.makeText(user_profile.this, "byza a3m", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    else{
                        Toast.makeText(user_profile.this, "failed to send the request", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
        if(currentstate.equals("req_sent")){
            friendrequest.child(currentuser.getUid()).child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    friendrequest.child(id).child(currentuser.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            currentstate="not friends";
                            button.setText("Send Friend Request");
                        }
                    });
                }
            });
        }
        if(currentstate.equals("req_recieved")){
           Mfriend=FirebaseDatabase.getInstance().getReference().child("friends");
          final  String date= DateFormat.getDateInstance().format(new Date());
            Mfriend.child(currentuser.getUid()).child(id).child("date").setValue(date).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                Mfriend.child(id).child(currentuser.getUid()).child("date").setValue(date).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            button.setText("unfriend this person");
                            button1.setVisibility(View.GONE);
                            currentstate="friends";
                            friendrequest.child(currentuser.getUid()).child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    friendrequest.child(id).child(currentuser.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                        }
                                    });
                                }
                            });

                        }
                    }
                });
                    }
                }
            });
        }
     if(currentstate.equals("friends")){
         Mfriend=FirebaseDatabase.getInstance().getReference().child("friends");
         Mfriend.child(currentuser.getUid()).child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
             @Override
             public void onComplete(@NonNull Task<Void> task) {
                 if(task.isSuccessful()){
                     Mfriend.child(id).child(currentuser.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                         @Override
                         public void onComplete(@NonNull Task<Void> task) {
                             if(task.isSuccessful()){
                                 Toast.makeText(user_profile.this, "sucessfully deleted", Toast.LENGTH_SHORT).show();
                                 currentstate="not friends";
                                 button.setText("SEND FRIEND REQUEST");
                             }
                         }
                     });
                 }
             }
         });

     }
    }
    public void decline(View v){
        friendrequest.child(currentuser.getUid()).child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                friendrequest.child(id).child(currentuser.getUid()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                     //   Toast.makeText(user_profile.this, "sucessfully declined", Toast.LENGTH_SHORT).show();
                        button1.setVisibility(View.GONE);
                        button.setText("SEND FRIEND REQUEST");
                        currentstate="not friends";
                    }
                });
            }
        });
    }
}
