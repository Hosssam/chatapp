package com.example.firebase;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class messageadapter extends RecyclerView.Adapter<messageadapter.messageholder> {
    public List<message> messages;
    public messageadapter(List<message>messages){
        this.messages=messages;
    }

    @NonNull
    @Override
    public messageholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row,parent,false);
        messageholder holder=new messageholder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final messageholder holder, int position) {
        holder.setIsRecyclable(false);
holder.msg.setText(messages.get(position).getMessage());
if(messages.get(position).getType().equals("text")){
   holder.upload.setVisibility(View.GONE);
}
else{
    holder.msg.setVisibility(View.GONE);
    Picasso.get().load(messages.get(position).getMessage()).into(holder.upload);


}
holder.time.setText(messages.get(position).getTime());
        FirebaseUser currentuser= FirebaseAuth.getInstance().getCurrentUser();
        String id=currentuser.getUid();
        String id2=messages.get(position).getTo();
        String id3=messages.get(position).getFrom();
if(id2.equals(id)){
    DatabaseReference chatuser= FirebaseDatabase.getInstance().getReference().child("users");
    holder.msg.setBackgroundResource(R.drawable.buttonstyle);
    chatuser.child(id3).addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            String image=dataSnapshot.child("image").getValue().toString();
            if(image.equals("default")){
                Picasso.get().load(R.drawable.images3).into(holder.pic);

            }
            else {
                Picasso.get().load(image).into(holder.pic);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    });


}
else {
    holder.pic.setVisibility(View.GONE);
    holder.msg.setBackgroundResource(R.drawable.msg_style);
    // holder.pic.setVisibility(View.GONE);
//DatabaseReference chatuser= FirebaseDatabase.getInstance().getReference().child("users");
//chatuser.child(currentuser.getUid()).addValueEventListener(new ValueEventListener() {
//    @Override
//    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//        String image=dataSnapshot.child("image").getValue().toString();
//        Picasso.get().load(image).into(holder.pic);
//    }
//
//    @Override
//    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//    }
//});}
}
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }


    public static class messageholder extends RecyclerView.ViewHolder{
        ImageView upload;
        ImageView pic;
        TextView msg;
        TextView time;
        RelativeLayout layout;
        public messageholder(@NonNull View itemView) {

            super(itemView);
           upload=itemView.findViewById(R.id.message1_upload);
            pic=itemView.findViewById(R.id.chat_picture);
            msg=itemView.findViewById(R.id.chat_message);
            time=itemView.findViewById(R.id.chat_time);
            layout=itemView.findViewById(R.id.messages_layout);
        }
    }
}



