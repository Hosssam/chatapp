package com.example.firebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }
    public void register(View v){
        Intent intent = new Intent(this,RegisterActivity.class);
        startActivity(intent);


    }
    public void Login(View v){
        Intent intent = new Intent(this,loginActivity.class);
        startActivity(intent);

    }
}
