package com.example.firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
FirebaseAuth mAuth;
EditText email,password,displayname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();

    }
    public void register(View v){
        email=findViewById(R.id.email_login);
        password=findViewById(R.id.password_login);
        displayname=findViewById(R.id.display);
       final ProgressDialog diag=new ProgressDialog(RegisterActivity.this);
        diag.setTitle("Register");
        diag.setMessage("Creating new Account");
        diag.show();
        if(!TextUtils.isEmpty(displayname.getText())){
       mAuth.createUserWithEmailAndPassword(email.getText().toString(),password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
           @Override
           public void onComplete(@NonNull Task<AuthResult> task) {
               if(task.isSuccessful()){
                   DatabaseReference mDatabase;
                   FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
                  String id= user.getUid();

                   mDatabase=FirebaseDatabase.getInstance().getReference().child("users").child(id);
                   HashMap<String,String> usermap= new HashMap<>();
                   usermap.put("Display",displayname.getText().toString());
                   usermap.put("Status","Hey,there");
                   usermap.put("image","default");
                   mDatabase.setValue(usermap).addOnCompleteListener(new OnCompleteListener<Void>() {
                       @Override
                       public void onComplete(@NonNull Task<Void> task) {
                           if(task.isSuccessful()){
                               diag.dismiss();
                               Intent go=new Intent(RegisterActivity.this,MainActivity.class);
                               go.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

                               startActivity(go);
                               RegisterActivity.this.finish();
                           }
                       }
                   });

               }
               else{
                   diag.dismiss();
                   Toast.makeText(RegisterActivity.this,"error while register",Toast.LENGTH_SHORT).show();
               }
           }
       });
    }
    else{
            Toast.makeText(this, "Please enter displayname", Toast.LENGTH_SHORT).show();}
    diag.dismiss();
    }
}
