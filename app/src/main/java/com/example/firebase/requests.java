package com.example.firebase;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class requests extends Fragment {
RecyclerView recycler;
FirebaseUser currentuser;
DatabaseReference requests;
DatabaseReference profile;
String id;
String pid;

    public requests() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_requests, container, false);
       recycler=v.findViewById(R.id.request_rec);
     recycler.setLayoutManager( new LinearLayoutManager(getContext()));
     currentuser= FirebaseAuth.getInstance().getCurrentUser();
     id=currentuser.getUid();
     requests=FirebaseDatabase.getInstance().getReference().child("Friendrequests").child(id);
     profile=FirebaseDatabase.getInstance().getReference().child("users");

       return v;
    }
    public void onStart(){
        super.onStart();

        FirebaseRecyclerAdapter<request,viewholder>adapter=new FirebaseRecyclerAdapter<request, viewholder>(
                request.class,
                R.layout.request_row,
                viewholder.class,
                requests
        ) {

            @Override
            protected void populateViewHolder(final viewholder viewholder, request request, int i) {
                viewholder.setIsRecyclable(false);
                pid=getRef(i).getKey();

                if(request.getRequest_type().equals("recieved")){

                    profile.child(pid).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                            String name=dataSnapshot.child("Display").getValue().toString();
                            viewholder.name.setText(dataSnapshot.child("Display").getValue().toString());
                            String image=dataSnapshot.child("image").getValue().toString();
                            if(image.equals("default"))
                            Picasso.get().load(R.drawable.images3).into(viewholder.pic);
                            else{
                                Picasso.get().load(image).into(viewholder.pic);
                            }
                            viewholder.status.setText(dataSnapshot.child("Status").getValue().toString());
                            viewholder.relat.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent= new Intent(getContext(),user_profile.class);
                                    intent.putExtra("user_id",dataSnapshot.getKey());
                                    getContext().startActivity(intent);
                                }
                            });
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                else{
                    viewholder.relat.setVisibility(View.GONE);
                }
            }
        };
    recycler.setAdapter(adapter);


    }
public static class viewholder extends RecyclerView.ViewHolder{
RelativeLayout relat;
ImageView pic;
TextView name;
TextView status;
    public viewholder(@NonNull View itemView) {
        super(itemView);
        relat=itemView.findViewById(R.id.request_relative);
        pic=itemView.findViewById(R.id.request_image);
        name=itemView.findViewById(R.id.request_name);
        status=itemView.findViewById(R.id.request_status);
    }
}
}
