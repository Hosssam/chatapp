package com.example.firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class Profile extends AppCompatActivity {
private FirebaseUser currentuser;
private DatabaseReference mDatabase;
private Button change_image;
private int gallerypic;
private StorageReference Mstorage;
private    TextView txt;
    private EditText txt1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        currentuser= FirebaseAuth.getInstance().getCurrentUser();
        String id= currentuser.getUid();
        final ImageView image=findViewById(R.id.profile_image);
        //image.setImageResource(R.drawable.ic_launcher_background);
        mDatabase=FirebaseDatabase.getInstance().getReference().child("users").child(id);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             //   String id2=dataSnapshot.getValue().toString();
                //Toast.makeText(Profile.this, id2, Toast.LENGTH_SHORT).show();
                txt=findViewById(R.id.profile_name);
             txt1=findViewById(R.id.profile_status);

                txt.setText(dataSnapshot.child("Display").getValue().toString());
                txt1.setText(dataSnapshot.child("Status").getValue().toString());
                String uri=dataSnapshot.child("image").getValue().toString();

                if(uri.equals("default")){
                //    Toast.makeText(Profile.this, "Please upload profile picture", Toast.LENGTH_SHORT).show();
                    Picasso.get().load(R.drawable.images3).into(image);
                }
                else{
                    Picasso.get().load(uri).into(image);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        change_image=findViewById(R.id.profile_change_image);
        change_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select_image"),gallerypic);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==gallerypic &&resultCode==RESULT_OK){
            Uri image =data.getData();
            Log.d("image",image.toString());
            Mstorage= FirebaseStorage.getInstance().getReference().child("profile_pictures").child(currentuser.getUid()+".jpg");
            Mstorage.putFile(image).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    if(task.isSuccessful()){

                        task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                Uri downloadurl=task.getResult();
                                mDatabase.child("image").setValue(downloadurl.toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                                Toast.makeText(Profile.this, "uploaded succesfully", Toast.LENGTH_SHORT).show();
                            }
                        });



                    }
                    else{
                        Toast.makeText(Profile.this, "not uploaded", Toast.LENGTH_SHORT).show();
                    }
                }
            });
           // Toast.makeText(this, image.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void update_status(View view) {
        String status=txt1.getText().toString();
        mDatabase.child("Status").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(Profile.this, "updated sucessfully", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
