package com.example.firebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class allusers extends AppCompatActivity {
RecyclerView recyclerView;
DatabaseReference mdatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allusers);
        recyclerView=findViewById(R.id.user_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    public void onStart(){
        super.onStart();
        mdatabase= FirebaseDatabase.getInstance().getReference().child("users");
        FirebaseRecyclerAdapter<user,userviewholder> firebaseRecyclerAdapter=new FirebaseRecyclerAdapter<user, userviewholder>(
                user.class,
                R.layout.user_row,
                userviewholder.class,
                mdatabase

        ) {

            @Override
            protected void populateViewHolder(userviewholder userviewholder, user user, int i) {

//String image=user.getImage();
                userviewholder.displayname.setText(user.getDisplay());
               // String name=user.getDisplay();
                //tring status=user.getStatus();
                userviewholder.status.setText(user.getStatus());
                if(!user.getImage().equals("default")){
                    Picasso.get().load(user.getImage()).into(userviewholder.user_image);
                }
                else{
                    Picasso.get().load(R.drawable.images3).into(userviewholder.user_image);
                }

            final    String user_id=getRef(i).getKey();
                userviewholder.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(allusers.this,user_profile.class);
                        intent.putExtra("user_id",user_id);
                        startActivity(intent);
                    }
                });
            }
        };



        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }
    public static class userviewholder extends RecyclerView.ViewHolder{
    TextView displayname;
    TextView status;
    ImageView user_image;
    RelativeLayout layout;

        public userviewholder(@NonNull View itemView) {
            super(itemView);
            displayname=itemView.findViewById(R.id.request_name);
            status= itemView.findViewById(R.id.request_status);
            user_image=itemView.findViewById(R.id.request_image);
            layout=itemView.findViewById(R.id.request_relative);
        }
    }
}
