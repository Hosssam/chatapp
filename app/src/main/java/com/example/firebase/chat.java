package com.example.firebase;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class chat extends Fragment {
DatabaseReference mydatabse;
DatabaseReference chat;
FirebaseUser currentuser;
String chatuser;
RecyclerView recycler;
String message;
    public chat() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_chat, container, false);
    currentuser= FirebaseAuth.getInstance().getCurrentUser();
    chat= FirebaseDatabase.getInstance().getReference().child("chat").child(currentuser.getUid());
    mydatabse=FirebaseDatabase.getInstance().getReference().child("messages").child(currentuser.getUid());
    recycler=v.findViewById(R.id.chat_recycler);
    recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        return v;
    }
    public void onStart(){
        super.onStart();
        FirebaseRecyclerAdapter<recentlytexted,viewholder>adapter=new FirebaseRecyclerAdapter<recentlytexted, viewholder>(
                recentlytexted.class,
                R.layout.recentlytexted,
                viewholder.class,
                chat
        ) {
            @Override
            protected void populateViewHolder(final viewholder viewholder, recentlytexted recentlytexted, int i) {
        chatuser=getRef(i).getKey();
        Query lastmsg=mydatabse.child(chatuser).limitToLast(1);

        lastmsg.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
              message=dataSnapshot.child("message").getValue().toString();
                viewholder.msg.setText(message);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
                DatabaseReference chatter=FirebaseDatabase.getInstance().getReference().child("users").child(chatuser);

                chatter.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        String image=dataSnapshot.child("image").getValue().toString();
                        String online=dataSnapshot.child("online").getValue().toString();
                        String Display=dataSnapshot.child("Display").getValue().toString();
                        viewholder.name.setText(Display);
                        viewholder.layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent= new Intent(getActivity(),chatActivity.class);
                                intent.putExtra("user_id",dataSnapshot.getKey());
                                getActivity().startActivity(intent);
                            }
                        });
                        if(online.equals("true")){
                            viewholder.online.setVisibility(View.VISIBLE);
                        }
                        else{
                            viewholder.online.setVisibility(View.GONE);
                        }
                        if(image.equals("default")){
                            Picasso.get().load(R.drawable.images3).into(viewholder.pic);
                        }
                        else{
                            Picasso.get().load(image).into(viewholder.pic);
                        }

                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        };
    recycler.setAdapter(adapter);
    }
public static class viewholder extends RecyclerView.ViewHolder{
ImageView pic;
TextView name;
TextView msg;
ImageView online;
RelativeLayout layout;
    public viewholder(@NonNull View itemView) {
        super(itemView);
            pic=itemView.findViewById(R.id.text_pic);
            name=itemView.findViewById(R.id.text_name);
            msg=itemView.findViewById(R.id.text_msg);
            online=itemView.findViewById(R.id.text_online);
            layout=itemView.findViewById(R.id.recentlytexted);
    }
}
}
