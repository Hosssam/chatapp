package com.example.firebase;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPageAdapter extends FragmentPagerAdapter {
    final List<Fragment> fragmentlist=new ArrayList<>();
    final List<String> fragmenttitle=new ArrayList<>();
    public ViewPageAdapter(FragmentManager   fm){
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        return fragmentlist.get(position);
    }

    @Override
    public int getCount() {
        return fragmenttitle.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmenttitle.get(position);
    }
    public void addFragment(Fragment fragment,String tittle){
        fragmentlist.add(fragment);
        fragmenttitle.add(tittle);
    }
}
