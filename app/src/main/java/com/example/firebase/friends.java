package com.example.firebase;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AlertDialogLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class friends extends Fragment {
private DatabaseReference mdatabase;
private View mainview;
RecyclerView recyclerView;
private DatabaseReference friend_user;
RecyclerView.LayoutManager layout;
FirebaseUser currentcuser;
String id;
    public friends() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mainview=inflater.inflate(R.layout.fragment_friends, container, false);
    recyclerView=mainview.findViewById(R.id.recycler1);
  currentcuser=FirebaseAuth.getInstance().getCurrentUser();
  id=currentcuser.getUid();
  friend_user=FirebaseDatabase.getInstance().getReference().child("users");
  mdatabase= FirebaseDatabase.getInstance().getReference().child("friends").child(id);
  recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return mainview;
    }
    public void onStart(){

        super.onStart();
        FirebaseRecyclerAdapter<friend,Friendsviewholder> adapter=new FirebaseRecyclerAdapter<friend, Friendsviewholder>(
                friend.class,
                R.layout.friend_row,
                Friendsviewholder.class,
                mdatabase
        ) {
            @Override
            protected void populateViewHolder(final Friendsviewholder friendsviewholder, friend friend, int i) {
          final String id=getRef(i).getKey();
          friend_user.child(id).addValueEventListener(new ValueEventListener() {
              @Override
              public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
          String   name=     dataSnapshot.child("Display").getValue().toString();
          String   status=     dataSnapshot.child("Status").getValue().toString();
          boolean    online=  (boolean) dataSnapshot.child("online").getValue();
         String      image=   dataSnapshot.child("image").getValue().toString();
                  friendsviewholder.name.setText(name);
                  friendsviewholder.status.setText(status);
                  if(image.equals("default")){
                      Picasso.get().load(R.drawable.images3).into(friendsviewholder.profile);
                  }
                  else{
                  Picasso.get().load(image).into(friendsviewholder.profile);}
                  if(online==true){
                      Picasso.get().load(R.drawable.greendot).into(friendsviewholder.online);
                      friendsviewholder.online.setVisibility(View.VISIBLE);
                  }
                  else{
                      friendsviewholder.online.setVisibility(View.GONE);
                  }
                  friendsviewholder.layout.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View view) {
                          CharSequence options[]=new CharSequence[]{"Open profile" ,"Send message"};
                          AlertDialog.Builder builder= new AlertDialog.Builder(getContext());
                          builder.setTitle("Select Option");
                          builder.setItems(options, new DialogInterface.OnClickListener() {
                              @Override
                              public void onClick(DialogInterface dialogInterface, int i) {
                                  if(i==0){
                                      Intent intent=new Intent(getActivity(),user_profile.class);
                                      intent.putExtra("user_id",id);
                                      getActivity().startActivity(intent);
                                  }
                                  else{
                                      Intent intent=new Intent(getActivity(),chatActivity.class);
                                      intent.putExtra("user_id",id);
                                      getActivity().startActivity(intent);
                                  }
                              }
                          });
                   builder.show();
                          //Toast.makeText(getContext(), "clicked", Toast.LENGTH_SHORT).show();
                      }
                  });
              }

              @Override
              public void onCancelled(@NonNull DatabaseError databaseError) {

              }
          });



            }
        };
        recyclerView.setAdapter(adapter);
    }
public static class Friendsviewholder extends RecyclerView.ViewHolder{
   RelativeLayout layout;
   ImageView profile;
   ImageView online;
   TextView name;
   TextView status;
    public Friendsviewholder(@NonNull View itemView) {
        super(itemView);
        profile=itemView.findViewById(R.id.friend_pic);
        online=itemView.findViewById(R.id.online);
        name=itemView.findViewById(R.id.friend_name);
        status=itemView.findViewById(R.id.friend_status);
        layout=itemView.findViewById(R.id.friend_re);
    }
}


}
