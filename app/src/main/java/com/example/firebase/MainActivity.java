package com.example.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
   private TabLayout tab;
    private ViewPager view;
    private DatabaseReference user;
    FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
      Toolbar toolbar = findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      tab=findViewById(R.id.tab);
      view=findViewById(R.id.viewpager);
      ViewPageAdapter adapter= new ViewPageAdapter(getSupportFragmentManager());
      adapter.addFragment(new chat(),"chat");
      adapter.addFragment(new requests(),"requests");
      adapter.addFragment(new friends(),"friends");
      view.setAdapter(adapter);
     tab.setupWithViewPager(view);
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        if(currentUser==null){
           moveHome();
        }
        else{
            user= FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
            user.child("online").setValue(true);
        }

    }
    public void onStop(){
        super.onStop();
        if(currentUser!=null) {
            user = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
            user.child("online").setValue(false);
        }
    }
    public void moveHome(){
        Intent intent= new Intent(this,HomeActivity.class);
        startActivity(intent);
        finish();
    }
    public boolean onCreateOptionsMenu(Menu menu){
            super.onCreateOptionsMenu(menu);
            getMenuInflater().inflate(R.menu.menu,menu);
            return true;
    }
    public boolean onOptionsItemSelected(MenuItem item){
        super.onOptionsItemSelected(item);
        if(item.getItemId()==R.id.logout){

moveHome();
FirebaseAuth.getInstance().signOut();
        }
        else if(item.getItemId()==R.id.profile){
            Intent intent = new Intent(MainActivity.this,Profile.class);
            startActivity(intent);
        }
        else if(item.getItemId()==R.id.allusers){
            Intent intent = new Intent(MainActivity.this,allusers.class);
            startActivity(intent);
        }
        return true;
    }
}
