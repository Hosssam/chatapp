package com.example.firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class chatActivity extends AppCompatActivity {
private String id;
DatabaseReference chat;
private Toolbar toolbar;
private String name;
private ImageView send;
private ImageView upload;
private  int gallerypick1;
private EditText txt;
private FirebaseUser currentuser;
private List<message> messages=new ArrayList<>();
private RecyclerView recyclerView;
private DatabaseReference msgdatabase;
private messageadapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_chat);
        id=getIntent().getStringExtra("user_id");
            toolbar=findViewById(R.id.chat_tool_bar);
       DatabaseReference mydatabase=FirebaseDatabase.getInstance().getReference().child("users");
        currentuser=FirebaseAuth.getInstance().getCurrentUser();

        recyclerView=findViewById(R.id.msgs_recycler);
       recyclerView.setLayoutManager(new LinearLayoutManager(this));
    adapter=new messageadapter(messages);
    recyclerView.setVerticalScrollBarEnabled(true);
    //recyclerView.getRecycledViewPool().setMaxRecycledViews(0,0);
    recyclerView.setAdapter(adapter);

    loadmsgs();

       mydatabase.child(id).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               name=dataSnapshot.child("Display").getValue().toString();
               toolbar.setTitle(name);


               upload=findViewById(R.id.chat_upload);
               txt=findViewById(R.id.chat_msg);

               setSupportActionBar(toolbar);

           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
     chat=FirebaseDatabase.getInstance().getReference().child("chat");

     chat.child(currentuser.getUid()).addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             if(!dataSnapshot.hasChild(id)){
                 HashMap<String,String> map=new HashMap<>();
                 map.put("connected","true");
                 chat.child(currentuser.getUid()).child(id).setValue(map);
                 chat.child(id).child(currentuser.getUid()).setValue(map);
             }
         }

         @Override
         public void onCancelled(@NonNull DatabaseError databaseError) {

         }
     });
     send=findViewById(R.id.chat_send);
    send.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            sendmessage();
            txt.setText("");
        }
    });
    upload=findViewById(R.id.chat_upload);
    upload.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent=new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"select image"),gallerypick1);
        }
    });
    }
    public void sendmessage(){
        String msg=txt.getText().toString();

        if(!TextUtils.isEmpty(msg)){
            DatabaseReference messages=FirebaseDatabase.getInstance().getReference().child("messages")
         .child(currentuser.getUid()).child(id).push();


            String msg_id=messages.getKey();
            DatabaseReference messages2=FirebaseDatabase.getInstance().getReference().child("messages").
                    child(id).child(currentuser.getUid()).child(msg_id);

            HashMap<String,String>messagemap=new HashMap<>();
            messagemap.put("message",msg);
            messagemap.put("type","text");
            messagemap.put("from",currentuser.getUid());
            messagemap.put("to",id);
            Calendar calendar= Calendar.getInstance();
            SimpleDateFormat format=new SimpleDateFormat("hh:mm aa");
            String time=format.format(calendar.getTime());
            messagemap.put("time",time);
            messages.setValue(messagemap);
            messages2.setValue(messagemap);
        }
    }
    public void loadmsgs(){
        msgdatabase=FirebaseDatabase.getInstance().getReference().child("messages").child(currentuser.getUid()).child(id);
        msgdatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                message message=dataSnapshot.getValue(message.class);
                messages.add(message);

                recyclerView.smoothScrollToPosition(messages.size());
                if(txt!=null) {
                    txt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           recyclerView.smoothScrollToPosition(messages.size());

                        }
                    });
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==gallerypick1&&resultCode==RESULT_OK){
            Uri imageurl=data.getData();
            DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference().child("messages").child(currentuser.getUid()).child(id).push();
            final String messageid=databaseReference.getKey();
            StorageReference storage= FirebaseStorage.getInstance().getReference().child("image_pics").child(messageid+".jpg");
            storage.putFile(imageurl).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
               if(task.isSuccessful()){
                   task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                       @Override
                       public void onComplete(@NonNull Task<Uri> task) {
                           if(task.isSuccessful()){
                               String imagedo=task.getResult().toString();
                               HashMap<String,String> imagehash=new HashMap<>();
                               imagehash.put("from",currentuser.getUid());
                               imagehash.put("to",id);
                               imagehash.put("type","image");
                               Calendar x= Calendar.getInstance();
                               SimpleDateFormat date=new SimpleDateFormat("hh:mm aa");
                               String time=date.format(x.getTime());
                               imagehash.put("time",time);
                               imagehash.put("message",imagedo);
                               DatabaseReference messages3=FirebaseDatabase.getInstance().getReference().child("messages")
                                       .child(currentuser.getUid()).child(id).child(messageid);



                               DatabaseReference messages4=FirebaseDatabase.getInstance().getReference().child("messages").
                                       child(id).child(currentuser.getUid()).child(messageid);
                               messages3.setValue(imagehash);
                               messages4.setValue(imagehash);
                           }
                       }
                   });
               }
                }
            });

        }
    }
}
